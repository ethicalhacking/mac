#!/bin/python3

import requests
import json


def get_mac_data():
    # mac = "BC:92:6B:A0:00:01"
    mac = input("Mac Address:\n")
    r   = requests.get('http://macvendors.co/api/' + mac)
    return r.content


def convert_json():
    data = get_mac_data()
    return json.loads(data.decode('utf-8'))


if __name__ == '__main__':
    data = convert_json()
    company = data['result']['company']
    print(company)
